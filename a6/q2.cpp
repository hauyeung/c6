#include <stdio.h>

void display(int arr[], int len);

int main(void)
{
	int i=0;
	int numarray[10] = {};
	while ( i < 10)
	{
		int num = 0;
		printf("Please enter a number: \n");
		scanf ("%i", &num);
		numarray[i] = num;
		i++;
	}
	display(numarray, i+1);
	return 0;
}

void display(int arr[], int len)
{
	int i = 0;
	while (i < 10)
	{
		printf("%i\n", arr[i]);
		i++;
	}
}