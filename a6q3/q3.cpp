#include <stdio.h>
#include <stdlib.h>

int sort_two(int* p1, int* p2);

int main(void)
{
	char n1[256], n2[256];
	int num1, num2, needsort;
	printf("Enter the first number: \n");
	gets(n1);
	num1 = atoi(n1);
	printf("Enter the second number:  \n");
	gets(n2);
	num2 = atoi(n2);
	needsort = sort_two(&num1, &num2);
	if (needsort == 1)
		printf("The program sorted the numbers.\n");
	else
		printf("The program didn't sort your numbers.\n");
	return 0;
}

int sort_two(int *p1, int* p2)
{
	int smallnum, bignum;
	if (*p1 < *p2)
	{
		smallnum = *p1, bignum = *p2;
		printf("%d \n%d\n", smallnum, bignum);
		return 0;
	}
	else
	{
		smallnum = *p2, bignum = *p1;
		printf("%d \n%d\n", smallnum, bignum);
		return 1;
	}

}